import { Injectable } from '@angular/core';
import { Product } from '../../models/product.model';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  products: Product[] = [];
  lastId: number = 0;

  constructor(private router: Router) { }

  getProducts(): Product[] {
    return this.products;
  }

  getProductById(id: number): Product | undefined {
    return this.products.find(product => product.id === id);
  }


  addProduct(product: Product): void {
    product.id = ++this.lastId;
    this.products.push(product);
    this.router.navigate(['/products']);
  }
  removeProduct(id: number): void {
    const index = this.products.findIndex(product => product.id === id);
    if (index !== -1) {
      this.products.splice(index, 1);
    }
  }

  updateProduct(product: Product): void {
    const index = this.products.findIndex(p => p.id === product.id);
    if (index !== -1) {
      this.products[index] = product;
    }
  }

  private generateProductId(): number {
    let id = 1;
    if (this.products.length > 0) {
      id = Math.max(...this.products.map(product => product.id)) + 1;
    }
    return id;
  }

}
