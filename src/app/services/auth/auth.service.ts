import { Injectable } from '@angular/core';
import { User } from '../../models/user.model';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  public loggedIn = false;

  private users: User[] = [
    { id: 1, name: 'Admin', username: 'admin', password: '123' },
    { id: 2, name: 'Jane', username: 'jane_doe', password: 'abcdef' },
    { id: 3, name: 'Bob', username: 'bob_smith', password: 'qwerty' },
  ];

  constructor(private router: Router) { }

  signup(name: string, username: string, password: string): boolean {
    const id = this.users.length + 1;
    this.users.push({ id, name, username, password });
    console.log('Usuário cadastrado: ', { id, name, username, password }); // <--- adiciona um log do usuário cadastrado
    console.log('Lista de usuários: ', this.users); // <--- adiciona um log da lista de usuários
    return true;
  }

  //retorna o array de usuários
  getUsers(): User[] {
    return this.users;
  }
  
  login(username: string, password: string): boolean {
    const user = this.users.find(
      (u) => u.username === username && u.password === password
    );
    if (user) {
      // Define o token no localStorage
      localStorage.setItem('token', 'JWT');

      // Define o estado de login como true
      this.loggedIn = true;

      return true;
    }
    return false;
  }

  logout(): void {
    // Remove o token do localStorage
    localStorage.removeItem('token');

    // Redireciona para a página de login
    this.router.navigate(['/login']);
    this.loggedIn = false;
  }

  getCurrentUser() {
    const token = localStorage.getItem('token');
    if (token) {
      const user = JSON.parse(atob(token.split('.')[1]));
      return user;
    } else {
      return null;
    }
  }

  isLoggedIn(): boolean {
    return !!localStorage.getItem('currentUser');
  }
  
}
