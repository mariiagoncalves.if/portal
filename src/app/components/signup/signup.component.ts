import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  name= '';
  username= '';
  password= '';

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  signup() {
    if (this.authService.signup(this.name, this.username, this.password)) {
      alert('Cadastro efetuado com sucesso');
      this.router.navigate(['/login']);
    } else {
      alert('Username já cadastrado');
    }
  }

  goToLogin() {
    this.router.navigate(['/login']);
  }

}
