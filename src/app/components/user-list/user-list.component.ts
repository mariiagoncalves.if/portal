import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  
  users: User[] = [];

  constructor(private authService: AuthService) {
  }
  ngOnInit(): void {
    this.users = this.authService.getUsers(); // <--- recupera a lista de usuários cadastrados
  }


}
