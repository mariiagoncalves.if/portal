import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../../services/product/product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent {
  name: string = '';
  price: number = 0;
  quant: number = 0;
  status: string = '';

  constructor(
    private productService: ProductService,
    private router: Router
  ) { }

  onSubmit() {
    this.productService.addProduct({
      id: 0, 
      name: this.name,
      price: this.price,
      description: '' ,
      quant: this.quant ,
      status: this.status ,
    });
    
    this.router.navigate(['/produtos']);
  }
}
