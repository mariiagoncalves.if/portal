import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  currentUser: any;

  constructor(public  authService: AuthService, private router: Router) {

   }

  ngOnInit(): void {
    this.currentUser = this.authService.getCurrentUser();
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
  onViewUsers() {
    this.router.navigate(['/users']);
  }
  home() {
    this.router.navigate(['/home']);
  }
  product() {
    this.router.navigate(['/produtos']);
  }

  onLogout() {
    this.authService.logout();
    this.router.navigate(['/login']),
      { queryParams: { message: 'Logout realizado com sucesso.' } };
  }
  
}
