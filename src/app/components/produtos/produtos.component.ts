import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.css']
})
export class ProdutosComponent implements OnInit {

  produtos = [
    { id: '1', name: 'Produto 1', price: 100, description: 'Descrição do produto 1', quant: 10, status: 'Ativo' },
    { id: '2', name: 'Produto 2', price: 554, description: 'Descrição do produto 2', quant: 20, status: 'Ativo' },
    { id: '3', name: 'Produto 3', price: 112, description: 'Descrição do produto 3', quant: 50, status: 'Ativo' },
    { id: '4', name: 'Produto 4', price: 65, description: 'Descrição do produto 4', quant: 102, status: 'Ativo' },
    { id: '5', name: 'Produto 5', price: 10, description: 'Descrição do produto 5', quant: 100, status: 'Ativo' },
    { id: '6', name: 'Produto 6', price: 660, description: 'Descrição do produto 6', quant: 102, status: 'Ativo' },
    { id: '7', name: 'Produto 7', price: 20, description: 'Descrição do produto 7', quant: 546, status: 'Ativo' },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
