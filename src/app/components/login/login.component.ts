import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  username = '';
  password = '';
  message: string = '';


  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.message = params['message'];
    });
  }

  login() {
    if (this.authService.login(this.username, this.password)) {
      
      this.router.navigate(['/produtos']);
    } else {
      alert('Login inválido');
    }
  }

  goToSignup() {
    this.router.navigate(['/signup']);
  }
}
